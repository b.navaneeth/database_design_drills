CREATE TABLE IF NOT EXISTS client(
    Client_id INT NOT NULL PRIMARY KEY,
    Client_name TEXT NOT NULL,
    Client_location TEXT 
);

CREATE TABLE IF NOT EXISTS manager(
    Manager_id INT NOT NULL PRIMARY KEY,
    Manager_name TEXT NOT NULL,
    Manager_loction TEXT
);

CREATE TABLE IF NOT EXISTS contracts(
    Contract_id INT NOT NULL PRIMARY KEY,
    Estimated_cost INT,
    Completion_date DATE
);

CREATE TABLE IF NOT EXISTS staff(
    Staff_id INT NOT NULL PRIMARY KEY,
    Staff_name TEXT,
    Staff_location TEXT,
    Manager_id INT,
    FOREIGN KEY (Manager_id) REFERENCES manager(Manager_id)
);

CREATE TABLE IF NOT EXISTS client_manager_staff(
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    Client_id INT,
    Manager_id INT,
    Contract_id INT,
    FOREIGN KEY (Client_id) REFERENCES client(Client_id),
    FOREIGN KEY (Manager_id) REFERENCES manager(Manager_id),
    FOREIGN KEY (Contract_id) REFERENCES contracts(Contract_id)
);
