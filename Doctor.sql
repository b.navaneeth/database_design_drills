CREATE TABLE IF NOT EXISTS Secretary(
    Secretary_id INT NOT NULL PRIMARY KEY,
    Secretary_name TEXT
);

CREATE TABLE IF NOT EXISTS Doctor(
    Doctor_id INT NOT NULL PRIMARY KEY,
    Doctor_name TEXT,
    Secretary_id INT,
    FOREIGN KEY (Secretary_id) REFERENCES Secretary(Secretary_id)
);

CREATE TABLE IF NOT EXISTS Patient(
    Patient_id INT NOT NULL PRIMARY KEY,
    Patient_name TEXT NOT NULL,
    Patient_DOB DATE,
    Patient_address TEXT
);

CREATE TABLE IF NOT EXISTS Prescription(
    Prescription_id INT NOT NULL PRIMARY KEY,
    Patient_id INT,
    Doctor_id INT,
    Prescription_date DATE,
    FOREIGN KEY (Patient_id) REFERENCES Patient(Patient_id),
    FOREIGN KEY (Doctor_id) REFERENCES Doctor(Doctor_id)
);

CREATE TABLE IF NOT EXISTS Drugs(
    Drug_id INT NOT NULL PRIMARY KEY,
    Drug_name TEXT
);

CREATE TABLE IF NOT EXISTS Prescription_drugs(
    Prescription_id INT,
    Drug_id INT,
    Dosage INT,
    FOREIGN KEY (Prescription_id) REFERENCES Prescription(Prescription_id),
    FOREIGN KEY (Drug_id) REFERENCES Drugs(Drug_id)
);