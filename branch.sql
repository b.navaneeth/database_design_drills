CREATE TABLE IF NOT EXISTS books(
    ISBN INT NOT NULL PRIMARY KEY,
    Title TEXT NOT NULL,
    Author TEXT,
    Publisher TEXT);

CREATE TABLE IF NOT EXISTS branch ( 
    Branch_id INT NOT NULL PRIMARY KEY, 
    Branch_Addr TEXT);

CREATE TABLE IF NOT EXISTS books_branch( 
    Branch_id INT, ISBN INT, 
    Num_copies INT, 
    FOREIGN KEY (Branch_id) REFERENCES branch(Branch_id), 
    FOREIGN KEY (ISBN) REFERENCES books(ISBN));